var Tiff = require('tiff.js');
var fs = require('fs');

var filename = process.argv[2];
var input = fs.readFileSync(filename);
var image = new Tiff({ buffer: input });
console.log(filename + ': width = ' + image.width() + ', height = ' + image.height());